mod utils;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct RGB {
    r: f32,
    g: f32,
    b: f32,
}

#[wasm_bindgen]
impl RGB {
    #[wasm_bindgen(constructor)]
    pub fn new(r: f32, g: f32, b: f32) -> RGB {
        RGB { r, g, b }
    }

    pub fn get_similar_color(&self, color_set: Vec<f32>) -> Vec<f32> {
        let mut found_rgb = &color_set[0..=2];
        let mut smallest_score = squred_difference(self, &found_rgb);

        for start_idx in (3..color_set.len()).step_by(3) {
            let current_rgb = &color_set[start_idx..=start_idx + 2];
            let current_score = squred_difference(self, current_rgb);

            if current_score < smallest_score {
                found_rgb = current_rgb;
                smallest_score = current_score;
            }
        }

        found_rgb.to_vec()
    }
}

#[wasm_bindgen]
pub fn squred_difference(rgb: &RGB, color: &[f32]) -> f32 {
    let squared_sum = ((rgb.r - color[0]) * (rgb.r - color[0]))
        + ((rgb.g - color[1]) * (rgb.g - color[1]))
        + ((rgb.b - color[2]) * (rgb.b - color[2]));

    (squared_sum as f32).sqrt()
}
