import * as utils from './utils';
import { Dimension } from './App';
import { RGB } from './entities/RGB';

describe('findAdjustableLength', () => {
    it('should throw an error if smallerLength is greate than equal to biggerLength', () => {
        expect(() => {
            utils.findAdjustableLength(3, 5);
        }).toThrow();

        expect(() => {
            utils.findAdjustableLength(3, 3);
        }).not.toThrow();

        expect(() => {
            utils.findAdjustableLength(5, 3);
        }).not.toThrow();
    });

    it('should return 0 if smallerLength is factor of biggerLength', () => {
        expect(utils.findAdjustableLength(6, 3)).toEqual(0);

        expect(utils.findAdjustableLength(1, 1)).toEqual(0);
    });

    it('should find less cost length to fit the biggerLength by smallerLength', () => {
        expect(utils.findAdjustableLength(5, 3)).toEqual(1);
        expect(utils.findAdjustableLength(7, 3)).toEqual(-1);
        expect(utils.findAdjustableLength(8, 3)).toEqual(1);

        expect(utils.findAdjustableLength(10, 9)).toEqual(-1);
        expect(utils.findAdjustableLength(10, 8)).toEqual(-2);
        expect(utils.findAdjustableLength(10, 7)).toEqual(-3);
    });
});

describe('to2DArray', () => {
    it('should check if imageData.data.length is multiple of 4', () => {
        const multipleOf4 = [
            [1, 2, 3, 4],
            [1, 2, 3, 4, 5, 6, 7, 8],
        ];
        const notMultipleOf4 = [[1], [1, 2], [1, 2, 3, 4, 5]];

        notMultipleOf4.forEach((arr) => {
            expect(() => {
                // eslint-disable-next-line
                utils.to2DArray({ data: arr } as any, { width: 8, height: 8 });
            }).toThrow();
        });

        multipleOf4.forEach((arr) => {
            expect(() => {
                // eslint-disable-next-line
                utils.to2DArray({ data: arr } as any, { width: 8, height: 8 });
            }).not.toThrow();
        });
    });

    it('should return 2D RGB[][] array', () => {
        const imageData = {
            data: [...Array.from(Array(64).keys())],
        };

        const dimension: Dimension = {
            width: 4,
            height: 4,
        };
        // eslint-disable-next-line
        const arr = utils.to2DArray(imageData as any, dimension);

        expect(arr).toBeTruthy();
        expect(arr.length).toEqual(dimension.height);

        for (const row of arr) {
            expect(row).toBeTruthy();
            expect(row.length).toEqual(dimension.width);
        }
    });
});

describe('adjust2DImageArray', () => {
    it('should throw error if any side of patchDimension is greater than imageDimension', () => {
        expect(() => {
            utils.adjust2DImageArray(
                [],
                { width: 2, height: 2 } as Dimension,
                { width: 2, height: 4 } as Dimension,
            );
        }).toThrow();

        expect(() => {
            utils.adjust2DImageArray(
                [],
                { width: 2, height: 2 } as Dimension,
                { width: 4, height: 2 } as Dimension,
            );
        }).toThrow();

        expect(() => {
            utils.adjust2DImageArray(
                [],
                { width: 2, height: 2 } as Dimension,
                { width: 4, height: 4 } as Dimension,
            );
        }).toThrow();
    });
});

describe('addPadding', () => {
    it('should add padding with correct length', () => {
        const imageArray: RGB[][] = [
            [new RGB(0, 0, 0), new RGB(0, 0, 0)],
            [new RGB(0, 0, 0), new RGB(0, 0, 0)],
        ];

        // to the 4x4 image array, add 3 padding to the width and 2 padding to its height
        const widthAdjustedArray = utils.addPadding(imageArray, 3, 'width');

        expect(widthAdjustedArray).toBeTruthy();
        expect(widthAdjustedArray.length).toEqual(2); // height
        expect(widthAdjustedArray[0].length).toEqual(5);

        const heightAdjustedArray = utils.addPadding(imageArray, 2, 'height');

        expect(heightAdjustedArray).toBeTruthy();
        expect(heightAdjustedArray.length).toEqual(4); // height
        expect(heightAdjustedArray[0].length).toEqual(2);
    });
});

describe('average', () => {
    it('should return RGB with zeros if the parameter, patch is an empty array', () => {
        const averaged = utils.average([]);

        expect(averaged.r).toEqual(0);
        expect(averaged.g).toEqual(0);
        expect(averaged.b).toEqual(0);
    });

    it('should return RGB with averaged RGB values within the patch', () => {
        const patch: RGB[][] = [
            [new RGB(2, 2, 2), new RGB(2, 2, 2)],
            [new RGB(1, 4, 3), new RGB(2, 3, 1)],
        ];

        const averaged = utils.average(patch);

        const expectedAverage: RGB = new RGB(1.75, 2.75, 2);

        // alpha value is not being used at the time of writing this
        expect(averaged.r).toEqual(expectedAverage.r);
        expect(averaged.g).toEqual(expectedAverage.g);
        expect(averaged.b).toEqual(expectedAverage.b);
    });
});

describe('flattenArray', () => {
    it('should faltten RGB[][] to 1-D array with each rgb values', () => {
        const array: RGB[][] = [
            [new RGB(1, 2, 3), new RGB(4, 5, 6)],
            [new RGB(7, 8, 9), new RGB(10, 11, 12)],
        ];

        const flattened = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        expect(utils.flattenArray(array)).toEqual(flattened);
    });
});
