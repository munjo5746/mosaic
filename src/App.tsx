import React from 'react';
import { fabric } from 'fabric';
import {
    H1,
    Button,
    Collapse,
    Slider,
    Switch,
    Classes,
    Icon,
    Divider,
    H5,
    Toaster,
} from '@blueprintjs/core';

import * as utils from './utils';
import { RGB } from './entities/RGB';

import DragAndDrop from './components/DragAndDrop';

import { drawPatchGuideLines, clearGuidelines, draw } from './utils.fabric';

import './App.scss';

export type Dimension = {
    width: number;
    height: number;
};

const App: React.FC = () => {
    const [imageArray, setImageArray] = React.useState<RGB[][]>([]);
    const [imageDimension, setImageDimension] = React.useState<Dimension>({
        width: 0,
        height: 0,
    });
    const [mosaicDimension, setMosaicDimension] = React.useState<Dimension>();
    const [patchDimension, setPatchDimension] = React.useState<Dimension>({
        width: 10,
        height: 10,
    });
    const [colorSet, setColroSet] = React.useState<RGB[]>([]);
    const [, setImageCanvas] = React.useState<fabric.Canvas>();
    const [sourceImageCanvas, setSourceImageCanvas] = React.useState<
        fabric.Canvas
    >();
    const [mosaicCanvas, setMosaicCanvas] = React.useState<fabric.Canvas>();

    const [openParameters, setOpenParameters] = React.useState<boolean>(true);
    const [showPatchGuideline, togglePatchGuideline] = React.useState<boolean>(
        false,
    );
    const [isRendering, setIsRendering] = React.useState<boolean>(false);

    const [wasm, setWasm] = React.useState<typeof import('mosaic-processor')>();
    React.useEffect(() => {
        // eslint-disable-next-line
        const loadWasm = async () => {
            const loadedWasm = await import('mosaic-processor');

            return loadedWasm;
        };

        loadWasm()
            .then((loadedWasm) => {
                setWasm(loadedWasm);
            })
            .catch(() => {
                Toaster.create().show({
                    message: `Failed to load wasm. Will proceed with javascript version.`,
                    intent: 'danger',
                    icon: 'warning-sign',
                    timeout: 3000,
                });
            });
    }, []);
    return (
        <div className="app">
            <div className="header">
                <H1>Mosaic</H1>
                <Icon icon="help" />
            </div>
            <div className="controls">
                <Button
                    outlined={false}
                    small={true}
                    rightIcon={'control'}
                    onClick={(): void => {
                        setOpenParameters(!openParameters);
                    }}
                >
                    Parameters
                </Button>
                <Collapse isOpen={openParameters}>
                    <div className="control-container">
                        <div className="patch-size">
                            {['width', 'height'].map((controlType) => (
                                <div
                                    key={`slider-control-${controlType}`}
                                    className="slider-control"
                                >
                                    <div className="label">{controlType}</div>
                                    <div className="slider">
                                        <Slider
                                            showTrackFill={false}
                                            min={1}
                                            max={15}
                                            value={
                                                controlType === 'width'
                                                    ? patchDimension.width
                                                    : patchDimension.height
                                            }
                                            onChange={(value): void => {
                                                if (controlType === 'width') {
                                                    setPatchDimension({
                                                        ...patchDimension,
                                                        width: value,
                                                    });
                                                } else {
                                                    setPatchDimension({
                                                        ...patchDimension,
                                                        height: value,
                                                    });
                                                }
                                            }}
                                            onRelease={(): void => {
                                                if (
                                                    !imageDimension ||
                                                    !patchDimension ||
                                                    !sourceImageCanvas
                                                ) {
                                                    // TODO: error
                                                    return;
                                                }

                                                if (!showPatchGuideline) return;

                                                drawPatchGuideLines({
                                                    canvas: sourceImageCanvas,
                                                    imageDimension,
                                                    patchDimension,
                                                });
                                            }}
                                        />
                                    </div>
                                </div>
                            ))}
                        </div>
                        <Switch
                            className={Classes.ALIGN_RIGHT}
                            disabled={true}
                            inline={true}
                            label="Guideline"
                            checked={showPatchGuideline}
                            onChange={(): void => {
                                if (!sourceImageCanvas) {
                                    return;
                                }

                                if (showPatchGuideline) {
                                    clearGuidelines(sourceImageCanvas);
                                } else {
                                    drawPatchGuideLines({
                                        canvas: sourceImageCanvas,
                                        imageDimension,
                                        patchDimension,
                                    });
                                }
                                togglePatchGuideline(!showPatchGuideline);
                            }}
                        />
                    </div>
                </Collapse>
                <Divider />
                <div className="image-cards-container">
                    {(['Image', 'Source Image', 'Draw', 'Output'] as Array<
                        'Image' | 'Source Image' | 'Draw' | 'Output'
                    >).map((type) => (
                        <div
                            key={`${type}`}
                            className={`${'image-card'} ${type
                                .toLowerCase()
                                .replace(/\s+/gi, '-')}`}
                        >
                            <div className="title">
                                <H5>{type}</H5>
                            </div>
                            <div className="content">
                                {type !== 'Output' && type !== 'Draw' && (
                                    <DragAndDrop
                                        onFileLoadComplete={(data): void => {
                                            const canvasDimension: Dimension = {
                                                width: 200,
                                                height: 200,
                                            };

                                            const image = new Image();
                                            image.onload = (): void => {
                                                const canvas = document.createElement(
                                                    'canvas',
                                                );
                                                const context = canvas.getContext(
                                                    '2d',
                                                );

                                                context?.drawImage(
                                                    image,
                                                    0,
                                                    0,
                                                    image.width,
                                                    image.height,
                                                    0,
                                                    0,
                                                    canvasDimension.width,
                                                    canvasDimension.height,
                                                );

                                                type === 'Image' &&
                                                    setImageDimension({
                                                        width:
                                                            canvasDimension.width,
                                                        height:
                                                            canvasDimension.height,
                                                    });

                                                // set fabric canvas
                                                const initialCanvas = new fabric.Canvas(
                                                    (type === 'Image' &&
                                                        'canvas') ||
                                                        'source-image-canvas',
                                                );
                                                initialCanvas.setDimensions({
                                                    width:
                                                        canvasDimension.width,
                                                    height:
                                                        canvasDimension.height,
                                                });
                                                initialCanvas.selection = false;

                                                const fabricImage = new fabric.Image(
                                                    image,
                                                );
                                                fabricImage.set({
                                                    top: 0,
                                                    left: 0,
                                                });

                                                fabricImage.scaleToWidth(
                                                    canvasDimension.width,
                                                );
                                                initialCanvas.setBackgroundImage(
                                                    fabricImage,
                                                    initialCanvas.renderAll.bind(
                                                        initialCanvas,
                                                    ),
                                                );

                                                type === 'Image'
                                                    ? setImageCanvas(
                                                          initialCanvas,
                                                      )
                                                    : setSourceImageCanvas(
                                                          initialCanvas,
                                                      );

                                                // set pixel data
                                                const imageData = context?.getImageData(
                                                    0,
                                                    0,
                                                    canvasDimension.width,
                                                    canvasDimension.height,
                                                );
                                                if (!imageData) return;

                                                const imageArray = utils.to2DArray(
                                                    imageData,
                                                    {
                                                        width:
                                                            canvasDimension.width,
                                                        height:
                                                            canvasDimension.height,
                                                    },
                                                );

                                                if (type === 'Image') {
                                                    const resizedArray = utils.adjust2DImageArray(
                                                        imageArray,
                                                        {
                                                            width:
                                                                canvasDimension.width,
                                                            height:
                                                                canvasDimension.height,
                                                        },
                                                        patchDimension,
                                                    );

                                                    setImageArray(resizedArray);

                                                    const resizedArrayDimension: Dimension = {
                                                        width:
                                                            resizedArray[0]
                                                                .length,
                                                        height:
                                                            resizedArray.length,
                                                    };
                                                    setMosaicDimension({
                                                        width:
                                                            resizedArrayDimension.width,
                                                        height:
                                                            resizedArrayDimension.height,
                                                    });

                                                    const mCanvas = new fabric.Canvas(
                                                        'mosaic',
                                                    );
                                                    mCanvas.setDimensions({
                                                        width:
                                                            resizedArrayDimension.width,
                                                        height:
                                                            resizedArrayDimension.height,
                                                    });

                                                    setMosaicCanvas(mCanvas);
                                                } else {
                                                    const imageArray = utils.to2DArray(
                                                        imageData,
                                                        {
                                                            width:
                                                                canvasDimension.width,
                                                            height:
                                                                canvasDimension.height,
                                                        },
                                                    );

                                                    const colorSet = utils.generateColorSetFromSourceImage(
                                                        imageArray,
                                                        {
                                                            width:
                                                                canvasDimension.width,
                                                            height:
                                                                canvasDimension.height,
                                                        } as Dimension,
                                                        patchDimension,
                                                    );

                                                    setColroSet(colorSet);
                                                }
                                            };

                                            image.src = data as string;
                                        }}
                                    ></DragAndDrop>
                                )}

                                {type === 'Draw' && (
                                    <div className="draw-button-container">
                                        <Button
                                            className="draw-button"
                                            loading={isRendering}
                                            rightIcon="circle-arrow-right"
                                            onClick={(): void => {
                                                if (
                                                    !imageArray ||
                                                    imageArray.length === 0
                                                )
                                                    return;
                                                if (
                                                    !mosaicDimension ||
                                                    !mosaicCanvas
                                                )
                                                    return;
                                                if (
                                                    !colorSet ||
                                                    colorSet.length === 0
                                                )
                                                    return;

                                                setIsRendering(true);
                                                // TODO: fix below without setTimeout...
                                                setTimeout(() => {
                                                    draw({
                                                        canvas: mosaicCanvas,
                                                        imageArray,
                                                        patchDimension,
                                                        colorSet,
                                                        wasm,
                                                    })
                                                        .then(() => {
                                                            setIsRendering(
                                                                false,
                                                            );
                                                        })
                                                        .catch(() => {
                                                            setIsRendering(
                                                                false,
                                                            );
                                                        });
                                                }, 500);
                                            }}
                                        >
                                            Draw
                                        </Button>
                                    </div>
                                )}

                                {type === 'Output' && (
                                    <div className="output-canvas-wrapper">
                                        <canvas
                                            id="mosaic"
                                            width={mosaicDimension?.width || 0}
                                            height={
                                                mosaicDimension?.height || 0
                                            }
                                        ></canvas>
                                    </div>
                                )}
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <div className="canvas-container">
                <div className="left">
                    <div className="canvas-wrapper">
                        <canvas id="canvas"></canvas>
                    </div>

                    <div className="canvas-wrapper">
                        <canvas id="source-image-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;
