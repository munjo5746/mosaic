import React from 'react';

import './DragAndDrop.scss';
import { Icon, H4, Toaster, ProgressBar } from '@blueprintjs/core';

type DragDropStatus = 'entered' | 'over' | 'left';

interface DragAndDropHandlers {
    onFileLoadComplete: (data: string | ArrayBuffer) => void;
}
const DragAndDrop: React.FC<DragAndDropHandlers> = (props) => {
    const [dragDropStatus, setDragDropStatus] = React.useState<
        DragDropStatus
    >();
    const [dragCounter, setDragCounter] = React.useState<number>(0);

    React.useEffect(() => {
        if (dragCounter === 0) setDragDropStatus('left');
    }, [dragCounter]);

    const [imageSrc, setImageSrc] = React.useState<string>();
    return (
        <div
            className={`dnd ${
                (dragDropStatus === 'entered' && 'active') || ''
            }`}
            onDragEnter={(e): void => {
                e.preventDefault();
                e.stopPropagation();

                setDragDropStatus('entered');
                setDragCounter(dragCounter + 1);
            }}
            onDragLeave={(e): void => {
                e.preventDefault();
                e.stopPropagation();

                setDragCounter(dragCounter - 1);
            }}
            onDragOver={(e): void => {
                e.preventDefault();
                e.stopPropagation();
            }}
            onDrop={(e): void => {
                e.preventDefault();
                e.stopPropagation();

                const { types, files } = e.dataTransfer;
                if (!types || !files || files.length === 0) return;
                if (files.length > 1) {
                    Toaster.create().show({
                        message: 'Multiple files are not supported yet.',
                        intent: 'warning',
                        icon: 'warning-sign',
                        timeout: 3000,
                    });

                    return;
                }

                const inValidFileTypeSet = new Set();
                for (let i = 0; i < files.length; i++) {
                    const file = files[i];
                    const extension = file.type.substring(
                        file.type.lastIndexOf('.') + 1,
                    );

                    if (!/(jpg|jpeg|png)/gi.test(extension))
                        inValidFileTypeSet.add(file.type);
                }

                if (inValidFileTypeSet.size > 0) {
                    Toaster.create().show({
                        message: `File types (${Array.from(inValidFileTypeSet)
                            .map((type) => type)
                            .join(', ')}) are not supported.`,
                        intent: 'danger',
                        icon: 'warning-sign',
                        timeout: 3000,
                    });

                    return;
                }

                const toaster = Toaster.create();
                const fileReader = new FileReader();
                fileReader.onloadstart = (): void => {
                    toaster.show(
                        {
                            icon: 'cloud-upload',
                            message: (
                                <ProgressBar value={0} intent={'primary'} />
                            ),
                        },
                        'progress',
                    );
                };
                fileReader.onprogress = (
                    event: ProgressEvent<FileReader>,
                ): void => {
                    const uploadPercentage = event.loaded / event.total;
                    toaster.show(
                        {
                            icon: 'cloud-upload',
                            message: (
                                <ProgressBar
                                    value={uploadPercentage}
                                    intent={
                                        uploadPercentage < 0
                                            ? 'primary'
                                            : 'success'
                                    }
                                />
                            ),
                        },
                        'progress',
                    );
                };
                fileReader.onload = (): void => {
                    setTimeout(() => {
                        toaster.clear();
                    }, 1000);

                    if (!fileReader || !fileReader.result) {
                        // TODO: handler error
                        return;
                    }

                    setImageSrc(fileReader.result as string);

                    props.onFileLoadComplete &&
                        props.onFileLoadComplete(fileReader.result);
                };
                fileReader.readAsDataURL(files[0]);
            }}
        >
            <div className="content-wrapper">
                {!imageSrc && (
                    <div className="dnd-sign">
                        <Icon icon="import" iconSize={23} color="#90ACB0" />
                        <H4>Drag N Drop</H4>
                    </div>
                )}

                {imageSrc && <img src={imageSrc} alt="image" />}
            </div>
        </div>
    );
};

export default DragAndDrop;
