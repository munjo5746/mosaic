import { fabric } from 'fabric';

import { Dimension } from './App';
import { RGB } from './entities/RGB';
import * as utils from './utils';

export const drawPatchGuideLines = ({
    canvas,
    imageDimension,
    patchDimension,
}: {
    canvas: fabric.Canvas;
    imageDimension: Dimension;
    patchDimension: Dimension;
}): void => {
    clearGuidelines(canvas);

    const lines: fabric.Line[] = [];
    for (let i = 0; i < imageDimension.width; i += patchDimension.width) {
        const points = [i, 0, i, canvas?.getHeight() || 0];
        const line = new fabric.Line(points, {
            stroke: 'red',
            hasBorders: false,
            hasControls: false,
        });

        lockObject(true, line);

        lines.push(line);
    }

    for (let i = 0; i < imageDimension.height; i += patchDimension.height) {
        const points = [0, i, canvas?.getWidth() || 0, i];
        const line = new fabric.Line(points, {
            stroke: 'red',
            hasBorders: false,
            hasControls: false,
        });

        lockObject(true, line);

        lines.push(line);
    }

    canvas?.add(...lines).renderAll();
};

export const clearGuidelines = (canvas: fabric.Canvas): void => {
    canvas.getObjects().forEach((obj) => {
        canvas.remove(obj).renderAll();
    });
};
export const draw = async ({
    canvas,
    imageArray,
    patchDimension,
    colorSet,
    wasm,
}: {
    canvas: fabric.Canvas;
    imageArray: RGB[][];
    patchDimension: Dimension;
    colorSet: RGB[];
    wasm?: typeof import('mosaic-processor');
}): Promise<void> => {
    canvas.clear();

    console.log(colorSet);
    const flattenedColorSet = new Float32Array(utils.flattenArray(colorSet));

    let x = 0;
    let y = 0;
    for (let i = 0; i < imageArray.length; i += patchDimension.height) {
        for (let j = 0; j < imageArray[i].length; j += patchDimension.width) {
            const patch = utils.getPatch(imageArray, i, j, patchDimension);
            const averaged = utils.average(patch);

            let color: RGB;
            if (wasm) {
                const rgb = new wasm.RGB(averaged.r, averaged.g, averaged.b);
                const [r, g, b] = Array.from(
                    rgb.get_similar_color(flattenedColorSet),
                );
                color = new RGB(r, g, b);
            } else {
                color = utils.findSimilarColor(averaged, colorSet);
            }

            drawPatch({
                canvas,
                at: { x, y },
                RGB: color,
                patchDimension,
            });
            x += patchDimension.width;
        }
        y += patchDimension.height;
        x = 0;
    }
};

export const drawPatch = ({
    canvas,
    at,
    RGB,
    patchDimension,
}: {
    canvas: fabric.Canvas;
    at: { x: number; y: number };
    RGB: RGB;
    patchDimension: Dimension;
}): void => {
    const rect = new fabric.Rect({
        top: at.y,
        left: at.x,
        width: patchDimension.width,
        height: patchDimension.height,
        fill: `rgb(${RGB.r},${RGB.g},${RGB.b})`,
        hasControls: false,
        hasBorders: false,
    });

    lockObject(true, rect);

    canvas.add(rect);
};

export const lockObject = (lock: boolean, object: fabric.Object): void => {
    object.set({
        lockMovementX: lock,
        lockMovementY: lock,

        lockScalingX: lock,
        lockScalingY: lock,

        lockUniScaling: lock,

        lockRotation: lock,
    });
};
