import { Dimension } from './App';
import { RGB } from './entities/RGB';

export const findAdjustableLength = (
    biggerLength: number,
    smallerLength: number,
): number => {
    if (smallerLength > biggerLength)
        throw new Error(
            `smallerLength = ${smallerLength} > ${biggerLength} = biggerLength`,
        );

    if (biggerLength % smallerLength === 0) return 0;

    let lengthTracker = 0;
    do {
        lengthTracker += smallerLength;
    } while (lengthTracker + smallerLength < biggerLength);

    const upperBound = lengthTracker + smallerLength;

    return upperBound - biggerLength < biggerLength - lengthTracker
        ? upperBound - biggerLength
        : lengthTracker - biggerLength;
};

export const findAdjustableDimension = (
    biggerDimension: Dimension,
    smallerDimension: Dimension,
): Dimension => {
    return {
        width: findAdjustableLength(
            biggerDimension.width,
            smallerDimension.width,
        ),
        height: findAdjustableLength(
            biggerDimension.height,
            smallerDimension.height,
        ),
    } as Dimension;
};

export const findMosaicCanvasDimension = (
    imageDimension: Dimension,
    patchDimension: Dimension,
): Dimension => {
    const adjustableDimension = findAdjustableDimension(
        imageDimension,
        patchDimension,
    );

    const { width, height } = imageDimension;
    const adjustedDimension: Dimension = {
        width: width + adjustableDimension.width,
        height: height + adjustableDimension.height,
    };
    const resizedImageDimension: Dimension = {
        width: adjustedDimension.width / patchDimension.width,
        height: adjustedDimension.height / patchDimension.height,
    };

    return resizedImageDimension;
};

export const to2DArray = (
    imageData: ImageData,
    dimension: Dimension,
): RGB[][] => {
    const { data } = imageData;

    // data must be ImageData
    // check if data is multiple of 4 (r,g,b and a)
    if (data.length % 4 !== 0)
        throw new Error(
            `the length of imageData.data must be multiple of 4, ${
                data.length
            } % 4 !== ${data.length % 4}`,
        );

    const imageArray: RGB[][] = [];
    let row: RGB[] = [];
    for (let i = 0; i < data.length; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];

        const rgb = new RGB(r, g, b);

        row.push(rgb);
        if (row.length === dimension.width) {
            imageArray.push(row);
            row = [];
        }
    }

    return imageArray;
};

export const adjust2DImageArray = (
    array: RGB[][],
    imageDimension: Dimension,
    patchDimension: Dimension,
): RGB[][] => {
    if (
        imageDimension.width < patchDimension.width ||
        imageDimension.height < patchDimension.height
    )
        throw new Error(
            `patch dimension(${patchDimension.width} x ${patchDimension.height}) cannot exceed image dimension(${imageDimension.width} x ${imageDimension.height}).`,
        );

    if (
        imageDimension.width % patchDimension.width === 0 &&
        imageDimension.height % patchDimension.height === 0
    ) {
        return array;
    }

    // Rule: negative number means to cut the original image with the length
    //       positive number means to expand the original image with the length

    let resizedArray: RGB[][] = [];
    const adjustableWidthLength = findAdjustableLength(
        imageDimension.width,
        patchDimension.width,
    );

    if (adjustableWidthLength !== 0) {
        resizedArray = addPadding(array, adjustableWidthLength, 'width');
    } else {
        resizedArray = [...array];
    }

    const adjustableHeightLength = findAdjustableLength(
        imageDimension.height,
        patchDimension.height,
    );

    if (adjustableHeightLength !== 0) {
        // ******** NOTE: here, the parameter is resizedArray from above...
        resizedArray = addPadding(
            resizedArray,
            adjustableHeightLength,
            'height',
        );
    }

    return resizedArray;
};

/**
 * This function adds padding with certain length to the image  2-d array.
 * Where to add the padding is determined with <orientation>.
 * @param imageArray 2-d image array
 * @param resizeLength the length to add the padding with. This can be negative if it has to cut off
 * @param orientation the orientation
 */
export const addPadding = (
    imageArray: RGB[][],
    resizeLength: number,
    orientation: 'width' | 'height',
): RGB[][] => {
    let resizedArray: RGB[][] = [];

    switch (orientation) {
        case 'width':
            if (resizeLength > 0) {
                // expand
                const padding: RGB[] = [];
                for (let i = 0; i < resizeLength; i++) {
                    padding.push(new RGB(0, 0, 0));
                }

                for (const row of imageArray) {
                    resizedArray.push(row.concat(padding));
                }
            } else {
                // shrink
                for (const row of imageArray) {
                    resizedArray.push(row.slice(0, row.length + resizeLength));
                }
            }
            break;
        case 'height':
            if (resizeLength > 0) {
                const width = imageArray[0].length;
                const padding: RGB[] = [];
                for (let i = 0; i < width; i++) {
                    padding.push(new RGB(0, 0, 0));
                }

                const padding2D = [];
                for (let i = 0; i < resizeLength; i++) {
                    padding2D.push(padding);
                }

                resizedArray = imageArray.concat(padding2D);
            } else {
                resizedArray = imageArray.slice(
                    0,
                    imageArray.length - resizeLength,
                );
            }
            break;
    }

    return resizedArray;
};

export const generateColorSetFromSourceImage = (
    image2DArray: RGB[][],
    imageDimension: Dimension,
    patchDimension: Dimension,
): RGB[] => {
    const adjustedArray = adjust2DImageArray(
        image2DArray,
        imageDimension,
        patchDimension,
    );

    const trackingSet = new Set<string>();
    const colorSet: RGB[] = [];
    for (let i = 0; i < imageDimension.height; i += patchDimension.height) {
        for (let j = 0; j < imageDimension.width; j += patchDimension.width) {
            const patch = getPatch(adjustedArray, i, j, patchDimension);
            const averaged = average(patch);

            const setKey = `${averaged.r}${averaged.g}${averaged.b}`;
            if (!trackingSet.has(setKey)) {
                trackingSet.add(setKey);
                colorSet.push(averaged);
            }
        }
    }

    return colorSet;
};

export const getPatch = (
    imageArray: RGB[][],
    rowIndex: number,
    columnIndex: number,
    patchDimension: Dimension,
): RGB[][] => {
    const patch: RGB[][] = [];
    for (let i = 0; i < patchDimension.height; i++) {
        const row = imageArray[rowIndex + i];

        if (!row) {
            break;
        }
        patch.push(row.slice(columnIndex, columnIndex + patchDimension.width));
    }

    return patch;
};

/**
 * This function adds all RGB values of RGB within a patch and calculates average.
 * The way it calculates the average RGB is it takes each row from the patch, add them all up
 * recursively until the end of the patch rows. And then, each r,g,b values  are divided by the total
 * number of RGB within the patch.
 * @param patch 2D RGB array.. indicating small section (square shape) of bigger 2D RGB
 */
export const average = (patch: RGB[][]): RGB => {
    let count = 0;
    let sum = new RGB(0, 0, 0);

    if (patch.length === 0) return sum;

    for (const row of patch) {
        count = count + row.length;

        sum = sum.add(addRecursively(row));
    }

    return new RGB(sum.r / count, sum.g / count, sum.b / count);
};

export const addRecursively = (row: RGB[]): RGB => {
    if (row.length === 0) return new RGB(0, 0, 0);

    return row[0].add(addRecursively(row.slice(1)));
};

export const findSimilarColor = (color: RGB, colorSet: RGB[]): RGB => {
    let foundColor = colorSet[0];
    let smallestScore = color.squaredDifference(foundColor);

    for (const c of colorSet.slice(1)) {
        const currentScore = color.squaredDifference(c);
        if (currentScore < smallestScore) {
            foundColor = c;
            smallestScore = currentScore;
        }
    }

    return foundColor;
};

export const flattenArray = (array: RGB[]): number[] => {
    let flattened: number[] = [];

    for (const rgb of array) {
        flattened = [...flattened, rgb.r, rgb.g, rgb.b];
    }

    return flattened;
};
