export class RGB {
    r: number;
    g: number;
    b: number;

    constructor(r: number, g: number, b: number) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    add = (to: RGB): RGB => {
        return new RGB(this.r + to.r, this.g + to.g, this.b + to.b);
    };

    squaredDifference = (from: RGB): number => {
        const sumOfSquares =
            Math.pow(this.r - from.r, 2) +
            Math.pow(this.g - from.g, 2) +
            Math.pow(this.b - from.b, 2);

        return Math.sqrt(sumOfSquares);
    };
}
